import os
#!/usr/bin/env python
# coding: utf-8



os.environ["CUDA_VISIBLE_DEVICES"]="1"


def ler_dados():
    import os
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt

    # ERROS
    from sklearn.metrics import explained_variance_score
    from sklearn.metrics import max_error
    from sklearn.metrics import mean_absolute_error
    from sklearn.metrics import mean_squared_error
    from sklearn.metrics import mean_squared_log_error
    from sklearn.metrics import median_absolute_error
    from sklearn.metrics import r2_score
    from sklearn.metrics import scorer

    dados = [] # Onde serao armazenados os dados
    path = ('/Users/danielfarias/Python/P&D_Ufal/Dados/Dados_Igor')
    #path = 'C:\\Users\Daniel Mello\\Documents\Python\\P&D CELESC\\Dados\\Dados_Igor'

    arquivos = []
    dados = []

    # r=root, d=directories, f = files
    for dirpath, dirnames, filenames in os.walk(path):
        for file in filenames:
            if '.dat' in file:
                arquivos.append(os.path.join(dirpath, file))

    arquivos = arquivos [1:]
    count = 0            
    dias_com_erro =[]

    for i in arquivos:
        #df = pd.read_table (arquivos[3], sep=",", names = ['TIMESTAMP' , 'RECORD' , 'Radiacao_Avg' , 'Temp_Cel_Avg' , 'Temp_Amb_Avg' , 'Tensao_S1_Avg' , 'Corrente_S1_Avg' , 'Potencia_S1_Avg' , 'Tensao_S2_Avg' , 'Corrente_S2_Avg' , 'Potencia_S2_Avg' , 'Potencia_FV_Avg' , 'Demanda_Avg' , 'FP_FV_Avg' , 'Tensao_Rede_Avg'],  header=3)
        #df = pd.read_table (i, sep=",", names = ['TIMESTAMP' , 'RECORD' , 'Radiacao_Avg' , 'Temp_Cel_Avg' , 'Temp_Amb_Avg' , 'Tensao_S1_Avg' , 'Corrente_S1_Avg' , 'Potencia_S1_Avg' , 'Tensao_S2_Avg' , 'Corrente_S2_Avg' , 'Potencia_S2_Avg' , 'Potencia_FV_Avg' , 'Demanda_Avg' , 'FP_FV_Avg' , 'Tensao_Rede_Avg'],  header=3)  
        df = pd.read_table (i , sep=",", header=3)
        df = df.iloc[:1440]
        #df_limitador = df.iloc[300:1080,:] # CRIA UMA OUTRA BASE DE DADOS COM OS VALORES ENTRE 5 E 18 HORAS
        df_limitador = df.iloc[360:1080,:] # CRIA UMA OUTRA BASE DE DADOS COM OS VALORES ENTRE 6 E 18 HORAS

        array_aux = np.array(df)
        if (array_aux.shape[1] == 15):
            df = df.drop(df.columns[[1]], axis=1) 
            df_limitador = df_limitador.drop(df_limitador.columns[[1]], axis=1) 

        #array_aux_limitador = np.array(df_limitador)
        #if (array_aux_limitador.shape[1] == 15):
            #df_limitador = df_limitador.drop(df_limitador.columns[[1]], axis=1) 

        # ARRUMA 2:  QUANDO NAO TEM TODOS OS DADOS DO DIA (EXCLUI O DIA)
        if (array_aux.shape[0] != 1440):
            dias_com_erro.append(array_aux[0,0][0:10]) # MOSTRA OS DIAS QUE TIVERAM ERRO (FORAM EXCLUIDOS)
            #print (array_aux.shape, "ERRO - ", array_aux[0,0][0:10])
        elif (array_aux[300:1080,1].mean() < 250):
            dias_com_erro.append(array_aux[0,0][0:10]) # MOSTRA OS DIAS QUE TIVERAM ERRO (FORAM EXCLUIDOS)
            #print (array_aux.shape, "ERRO - ", array_aux[0,0][0:10])
        else:
            #print (array_aux.shape)       
            if count ==0:        
                tabela_2d = np.array(df)
                tabela_3d = np.array(df)
                tabela_2d_limitador = np.array(df_limitador)
                tabela_3d_limitador = np.array(df_limitador)
            print ("Processo : " ,round (count/len(arquivos)*100, 2), " % ") # MOSTRA O PROCESSAMENTO DOS DADOS COM 2 CASAS DECIMAIS
            #print (tabela[0:60,0])
            if count > 0:
                tabela_3d = np.dstack((tabela_3d, np.array(df))) # DEIXA EM 3D
                tabela_2d = np.concatenate((tabela_2d,  np.array(df))) # JUNTA TUDO EM 2D
                tabeltabela_3d_limitadora_3d = np.dstack((tabela_3d_limitador, np.array(df_limitador))) # DEIXA EM 3D
                tabela_2d_limitador = np.concatenate((tabela_2d_limitador,  np.array(df_limitador))) # JUNTA TUDO EM 2D
            count = count+1

    # TRANSFORMA OS DADOS EM UM DATAFRAME PANDAS
    tabela = pd.DataFrame(data=tabela_2d, columns=['Horario' , 'Radiacao_Avg' , 'Temp_Cel_Avg' , 'Temp_Amb_Avg' , 'Tensao_S1_Avg' , 'Corrente_S1_Avg' , 'Potencia_S1_Avg' , 'Tensao_S2_Avg' , 'Corrente_S2_Avg' , 'Potencia_S2_Avg' , 'Potencia_FV_Avg' , 'Demanda_Avg' , 'FP_FV_Avg' , 'Tensao_Rede_Avg'])
    tabela_limitador = pd.DataFrame(data=tabela_2d_limitador, columns=['Horario' , 'Radiacao_Avg' , 'Temp_Cel_Avg' , 'Temp_Amb_Avg' , 'Tensao_S1_Avg' , 'Corrente_S1_Avg' , 'Potencia_S1_Avg' , 'Tensao_S2_Avg' , 'Corrente_S2_Avg' , 'Potencia_S2_Avg' , 'Potencia_FV_Avg' , 'Demanda_Avg' , 'FP_FV_Avg' , 'Tensao_Rede_Avg'])

    # Retira os dados entre 28/02 e 06/03 queo com Radiacao_Avg sempre 0
    indices = []
    for i in range (77760 - 67680):
        indices.append(i+67680)

    indices_limitador = []
    for i in range (42120 - 36660):
        indices_limitador.append(i+36660)  

    ###############################################################################################################################
                                                # AJUSTES FINOS:

    # FONTE: https://kanoki.org/2019/07/17/pandas-how-to-replace-values-based-on-conditions/
    tabela.loc[(tabela.Radiacao_Avg < 0 ),'Radiacao_Avg'] = 0 # A COLUNA RADIACAO_AVG SE FOR MENOR QUE 0 RECEBER 0 PARA ELIMINAR ERROS DO SENSOR
    tabela_limitador.loc[(tabela_limitador.Radiacao_Avg < 0 ),'Radiacao_Avg'] = 0 # A COLUNA RADIACAO_AVG SE FOR MENOR QUE 0 RECEBER 0 PARA ELIMINAR ERROS DO SENSOR


    tabela_nova = tabela.drop(indices) # CRIA UMA NOVA TABELA QUE ELIMINA OS ERROS ENTRE 28/02 e 06/03 QUE A RADIACAO SEMPRE 0
    tabela_nova_limitador = tabela_limitador.drop(indices_limitador) # CRIA UMA NOVA TABELA QUE ELIMINA OS ERROS ENTRE 28/02 e 06/03 QUE A RADIACAO SEMPRE 0
    #tabela_nova_limitador = tabela_nova_limitador.drop(indices_limitador_2) # CRIA UMA NOVA TABELA QUE ELIMINA OS ERROS ENTRE 28/02 e 06/03 QUE A RADIACAO SEMPRE 0

    dados = np.array (tabela_nova) # OS DADOS FICAM NO FORMATO 2D EM ARRAY
    dados_limitador = np.array (tabela_nova_limitador) # OS DADOS FICAM NO FORMATO 2D EM ARRAY

        
    
    # CRIA UMA LISTA COM OS VALORES QUE DEVEM SER EXCLUIDOS, CADA DIA FOI COLETADO POR CADA VARIAVEL (COLUNA)
    # NAO EXISTE DIA 08-03 (arquivo[57]) e 16-02 (arquivo [37]) -  dias com erro (dias_com_erro)

    lista_exclusao = ['01-15','01-25', '01-27', '01-28', '01-29', '01-30', '01-31', '02-01', '02-02', '02-03', '02-04', '02-05', '02-06', '02-07', '02-08', '02-09', '02-10', '02-11', '02-12', '02-13', '02-14', '02-15' , '02-16', '02-17', '03-07','03-08', '03-09','06-07' ,'06-11', '12-12', '12-15', '12-17', '12-19'] 
    index_lista_exclusao = []
    
    print ("COMEcOU PROCESSO DE EXCLUSaO")
    for i in range (len (tabela_limitador)):
        for j in range (len (lista_exclusao)):
            if tabela_limitador['Horario'][i][5:10] == lista_exclusao[j]:
                #print (i)
                index_lista_exclusao.append (i)

           
        
    print ("\nCOMEcOU PROCESSO DE EXCLUSaO")    
    # CRIA UMA NOVA TABELA COM OS VALORES EXCLUIDOS
    nova_tabela_lista = []

    for i in range (len (tabela_2d_limitador)):
        if i not in index_lista_exclusao:
            nova_tabela_lista.append (tabela_2d_limitador[i])
            
    print ("\nDADOS CARREGADOS COM SUCESSO... FIM!!!")    
    return (nova_tabela_lista)
    # Mostra a o diio + nome do arquivo            
    #for filenames in arquivos:
        #print(filenames)


# In[3]:
import pandas as pd
import numpy as np
#dados = ler_dados()

# Transforma o arquivo no formato Pandas
#dados = pd.DataFrame(data=dados, columns=['Horario' , 'Radiacao_Avg' , 'Temp_Cel_Avg' , 'Temp_Amb_Avg' , 'Tensao_S1_Avg' , 'Corrente_S1_Avg' , 'Potencia_S1_Avg' , 'Tensao_S2_Avg' , 'Corrente_S2_Avg' , 'Potencia_S2_Avg' , 'Potencia_FV_Avg' , 'Demanda_Avg' , 'FP_FV_Avg' , 'Tensao_Rede_Avg'])

# CARREGA OS DADOS DO EXCEL
dados_excel = pd.read_csv ('Dados_txt/dados.csv')
dados = dados_excel.iloc[:,1:]


# In[4]:


# CRIA NOVA COLUNA COM OS VALORES DAS FAIXAS DE TENSAO 

# TENSAO     - VALOR (CATEGORIA)
#  <231      - Normal   (-1)
# <=231 <242 - Precaria (0)
# >=242      - Critrica (1)


# # CRIA AS FU

# In[6]:


########################################################################################################################
    
                        # CRIA FUNCAO QUE SEPARA OS DADOS DE TREINO E TESTE
    
########################################################################################################################
def split_sequence(sequence, n_steps, forecast):
	X, y = list(), list()
	for i in range(len(sequence)):
		# find the end of this pattern
		end_ix = i + n_steps
		# check if we are beyond the sequence
		if end_ix +forecast > len(sequence):
			break
		# gather input and output parts of the pattern
		seq_x, seq_y = sequence[i:end_ix], sequence[end_ix:end_ix+forecast]
		X.append(seq_x)
		y.append(seq_y)
	return np.array(X), np.array(y)


# In[ ]:





# In[7]:


########################################################################################################################
                        # CRIA FUNCAO PARA NORMALIZAR OS DADOS
########################################################################################################################

def normalizar (dados):
    from sklearn.preprocessing import MinMaxScaler
    dados = dados.astype ('float32')
    scaler_dados = MinMaxScaler (feature_range=(0,1))
    dados_normalizados = scaler_dados.fit_transform (dados)
    return (dados_normalizados, scaler_dados)
    


# In[8]:


########################################################################################################################
                        # CRIA FUNCAO PARA PLOT OS GRaFICOS
########################################################################################################################

def grafico(variavel):
    import matplotlib.pyplot as plt
    plt.figure(figsize=(16,8))
    plt.plot (variavel)


# In[9]:


########################################################################################################################
                    # FUNCAO PARA SEPARAR OS DADOS EM TREINO E TESTE DA LSTM
########################################################################################################################

def separar_dados_lstm (dados, dados_lidos, dados_predito, porcentagem_test):
    test_size = int (len (dados)*porcentagem_test)
    train_size = int (len(dados) - test_size)
    train, test = dados[0:train_size], dados[train_size:len(dados)]

    #print ('Train_Shape: ', train.shape)
    #print ('Test_Shape: ', test.shape)
    
    X_train, y_train = split_sequence(train, dados_lidos, dados_predito)
    X_test, y_test = split_sequence(test, dados_lidos, dados_predito)

    X_train = X_train.reshape (X_train.shape[0], X_train.shape[1], 1)
    y_train = y_train.reshape (y_train.shape[0], y_train.shape[1], 1)
    X_test = X_test.reshape (X_test.shape[0], X_test.shape[1], 1)
    y_test = y_test.reshape (y_test.shape[0], y_test.shape[1], 1)
    
    return (X_train, y_train, X_test, y_test)


# In[10]:


def separar_dados_cnn (dados, dados_lidos, dados_predito, porcentagem_test):
    
    test_size = int (len (dados)*porcentagem_test)
    train_size = int (len(dados) - test_size)
    train, test = dados[0:train_size], dados[train_size:len(dados)]
    
    #dados_lidos = 20
    #dados_predito = 1
    n_features = 1
    n_steps_sub = 2
    n_seq_sub = int (dados_lidos/n_steps_sub)

    X_train, y_train = split_sequence(train, dados_lidos, dados_predito)
    X_test, y_test = split_sequence(test, dados_lidos, dados_predito)

    X_train = X_train.reshape((X_train.shape[0], n_seq_sub,1, n_steps_sub, n_features))
    X_test = X_test.reshape((X_test.shape[0], n_seq_sub,1, n_steps_sub, n_features))
    y_train = y_train.reshape(y_train.shape[0],-1)
    y_test = y_test.reshape(y_test.shape[0], -1)
    #print ('Dados de Treino e Testes FORMATADOS da CNN')
    #print ('X_train_SHAPE:', X_train.shape)
    #print ('Y_train_SHAPE:', y_train.shape)

    #print ('X_test_SHAPE:', X_test.shape)
    #print ('Y_test_SHAPE:', y_test.shape)
    return (X_train, y_train, X_test, y_test)


# In[11]:


########################################################################################################################
                        # CRIA A FUNCAO DE SEPERAR OS DADOS MULTV
########################################################################################################################

def separar_dados_multvariavel(sequences, n_steps, forecast):
	X, y = list(), list()
	for i in range(len(sequences)):
		# find the end of this pattern
		end_ix = i + n_steps
#		print (end_ix)        
		# check if we are beyond the dataset
		if end_ix + forecast > len(sequences):
#			print ('fim')            
			break
		# gather input and output parts of the pattern
		#seq_x, seq_y = sequences[i:end_ix, :-1], sequences[end_ix, -1]
		seq_x, seq_y = sequences[i:end_ix, :-1], sequences[end_ix:end_ix + forecast, -1]
#		print (seq_x)
#		print (seq_y)
		X.append(seq_x)
		y.append(seq_y)
	return np.array(X), np.array(y)

'''
def separar_dados_multvariavel(sequences, n_steps, forecast):
	X, y = list(), list()
	for i in range(len(sequences)):
		# find the end of this pattern
		end_ix = i + n_steps
#		print (end_ix)        
		# check if we are beyond the dataset
		if end_ix + forecast > len(sequences):
#			print ('fim')            
			break
		# gather input and output parts of the pattern
		seq_x, seq_y = sequences[i:end_ix, :-1], sequences[end_ix:end_ix + forecast, -1]
#		seq_x, seq_y = sequences[i:end_ix, :-1], sequences[end_ix, -1]
#		print (seq_x)
#		print (seq_y)
		X.append(seq_x)
		y.append(seq_y)
	return np.array(X), np.array(y)
# In[12]:
'''

########################################################################################################################
                        # CRIA A FUNCAO DE SEPERAR OS DADOS MULTVARARA CNN
########################################################################################################################

def separar_multdados_cnn (dados, dados_lidos, dados_predito, n_features, porcentagem_test):
    
    test_size = int (len (dados)*porcentagem_test)
    train_size = int (len(dados) - test_size)
    train, test = dados[0:train_size], dados[train_size:len(dados)]
    
    #dados_lidos = 20
    #dados_predito = 1
    #n_features = 1
    n_steps_sub = 2
    n_seq_sub = int (dados_lidos/n_steps_sub)

    X_train, y_train = split_sequence(train, dados_lidos, dados_predito)
    X_test, y_test = split_sequence(test, dados_lidos, dados_predito)

    X_train = X_train.reshape((X_train.shape[0], n_seq_sub,1, n_steps_sub, n_features))
    X_test = X_test.reshape((X_test.shape[0], n_seq_sub,1, n_steps_sub, n_features))
    #X_test = []
    
    y_train = y_train.reshape(y_train.shape[0],-1)
    y_test = y_test.reshape(y_test.shape[0], -1)
    y_train = y_train[:, -1]
    y_test = y_test[:, -1]
    
    #print ('Dados de Treino e Testes FORMATADOS da CNN')
    #print ('X_train_SHAPE:', X_train.shape)
    #print ('Y_train_SHAPE:', y_train.shape)

    #print ('X_test_SHAPE:', X_test.shape)
    #print ('Y_test_SHAPE:', y_test.shape)
    return (X_train, y_train, X_test, y_test)


# In[13]:


########################################################################################################################
                        # CRIA A FUNCAO DE TREINAR A REDE
########################################################################################################################

def treinar_rede_bidirecional (X_train, y_train, dados_lidos,dados_predito):
    # Modeling & Preprocessing
    
    #from tensorflow.keras.layers import Conv2D, BatchNormalization, Activation, Flatten, Dense, Dropout, LSTM, Embedding, Bidirectional
    #from tensorflow.keras import initializers, Model, optimizers, callbacks
    #from tensorflow.keras.models import load_model, Sequential
    #from tensorflow.keras.callbacks import Callback
    #from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score
    
    from tensorflow.keras.layers import Conv2D, BatchNormalization, Activation, Flatten, Dense, Dropout, LSTM, Input, TimeDistributed, Embedding, Bidirectional
    from tensorflow.keras import initializers, Model, optimizers, callbacks
    from tensorflow.keras.models import load_model, Sequential
    from tensorflow.keras.callbacks import Callback
    from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score


    model = Sequential()
    model.add(Bidirectional(LSTM(100, return_sequences=True), input_shape=(X_train.shape[1], X_train.shape[2])))
    model.add(Dropout(0.2))
    model.add(Bidirectional(LSTM(50, return_sequences=True)))
    model.add(Dropout(0.2))    
    model.add(Bidirectional(LSTM(30, return_sequences=True)))
    model.add(Dropout(0.2))    
    model.add(Bidirectional(LSTM(10)))
    model.add(Dropout(0.2))
    model.add(Dense(y_train.shape[1]))
    model.compile(optimizer='adam', loss='mse')
    print (model.summary())


    print ('STEPS: ', dados_lidos)
    print ('FORECAST: ', dados_predito)

    # fit model
    epocas = 12
    model.fit(X_train, y_train, epochs=epocas, verbose=2)
    return (model)


# In[14]:


def treinar_rede_cnn (X_train, y_train, dados_lidos,dados_predito, n_features):
    from tensorflow.keras.models import Sequential
    from tensorflow.keras.layers import LSTM, Dense, Flatten, ConvLSTM2D, Conv1D, Conv2D, BatchNormalization, Activation, Dropout, TimeDistributed, Embedding, MaxPooling1D
    
    #n_features = 1
    n_steps_sub = 2
    n_seq_sub = int (dados_lidos/n_steps_sub)

    model = Sequential()
    model.add(ConvLSTM2D(filters=64, kernel_size=(1,n_steps_sub), activation='relu', input_shape=(n_seq_sub, 1, n_steps_sub, n_features)))
    model.add(Flatten())
    model.add(Dense(dados_predito))
    model.compile(optimizer='adam', loss='mse')
    model.summary()
    
    print ('STEPS: ', dados_lidos)
    print ('FORECAST: ', dados_predito)

    # TREINO DA REDE
    epocas = 12
    model.fit(X_train, y_train, epochs=epocas, verbose=2)
    return (model)


# In[15]:


########################################################################################################################
                    # Funcao para calcular o MAPE (MEAN ABSOLUT PERCENT ERROR)
########################################################################################################################

def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100



def calcular_erros (test,predictions):
    from sklearn.metrics import explained_variance_score
    from sklearn.metrics import max_error
    from sklearn.metrics import mean_absolute_error
    from sklearn.metrics import mean_squared_error
    from sklearn.metrics import mean_squared_log_error
    from sklearn.metrics import median_absolute_error
    from sklearn.metrics import r2_score
    from sklearn.metrics import scorer
    import math


    score_variance = explained_variance_score(test, predictions)# Explained variance regression score function
    erro_absoluto_medio = mean_absolute_error(test, predictions) #Mean absolute error regression loss
    erro_medio_quadratico = mean_squared_error(test, predictions) #Mean squared error regression loss
    rmse = math.sqrt (erro_medio_quadratico) # Calculo do RMSE
    mape = mean_absolute_percentage_error(test, predictions) # Calcula o MAPE  ( a %)
    erro_mediana = median_absolute_error(test, predictions)# Median absolute error regression loss
    erro_r2 = r2_score(test, predictions) #R^2 (coefficient of determination) regression score function.

    # Cria a lista com os erros
    lista_erros = []
    lista_erros.append (score_variance)
    lista_erros.append (erro_absoluto_medio)
    lista_erros.append (erro_medio_quadratico)
    lista_erros.append (rmse)
    lista_erros.append (mape)
    lista_erros.append (erro_mediana)
    lista_erros.append (erro_r2)


    print('\nERROS:\nScore Variance: %.3f' % score_variance,
            '\nErro Absoluto Medio: %.3f' %erro_absoluto_medio, '\nErro Medio Quadratico: %.3f' %erro_medio_quadratico,
            '\nMAPE: %.3f' %mape,'\nRMSE: %.3f' %rmse, '\nErro Mediano Absoluto: %.3f' %erro_mediana, 
            '\nErro R2: %.3f' %erro_r2)
    
    return (lista_erros)


# In[16]:


#######################################################################################################################
#
#                             FUNCAO QUE SALVA OS MODELOS
#
#FONTE: https://www.dobitaobyte.com.br/como-salvar-um-model-treinado-com-keras/
########################################################################################################################
def salvar_modelo(nome_modelo, modelo):
    #nome_modelo = 'modelo_bidirectional_10_radiacao_'+nova_tabela_pandas.columns[1]+'_' + str(n_steps) + '_' + str(forecast)
    model_json = modelo.to_json()
    with open(nome_modelo, "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    modelo.save_weights(nome_modelo+'.h5')
    print('MODELO SALVO\nNome do Modelo: ', nome_modelo)


# In[17]:


#######################################################################################################################
#
#                             FUNCAO QUE CARREGA O MODELO TREINANDO
#
########################################################################################################################
dados_lidos = 20
dados_predito = 1
arquivo_modelo = 'Modelos/modelo_bidirecional_multvariado_camada_3_lendo_'+str (dados_lidos)+'_predicao_'+str (dados_predito)
    
def carregar_modelo (arquivo_modelo, dados_lidos, dados_predito):
    from tensorflow.keras.models import model_from_json
    #dados_lidos = 20 # SELECIONA O ARQUIVO A SER LIDO (DIAS LIDOS)
    #dados_predito = 1 # SELECIONA O ARQUIVO A SER LIDO (DIAS PREDICAO)

    #arquivo_modelo = 'Modelos/modelo_bidirecional_multvariado_camada_3_lendo_'+str (dados_lidos)+'_predicao_'+str (dados_predito)
    #arquivo_modelo = 'modelo_10_2.json'
    #json_file = open(arquivo_modelo+'.json', 'r')
    json_file = open(arquivo_modelo, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    loaded_model.summary()
    # load weights into new model
    loaded_model.load_weights(arquivo_modelo+'.h5')
    print('REDE CARREGADA COM SUCESSO: ', arquivo_modelo , '\n\n')
    return (loaded_model)


# # INICIO DO PROGRAMA MESMO

# In[18]:


# Calcula a normalizacao dos dados.....

from sklearn.preprocessing import MinMaxScaler
dados_radiacao = dados.iloc[:,1:2] # Pega a Radi
dados_temp_celula = dados.iloc[:,2:3] # Pega a Temperatura da Celula
#dados_temp_Amb = dados.iloc[:,3:4] # Pega a Temperatura Ambiente (Muitos dados com erro)
dados_demanda = dados.iloc[:,11:12] # Pega a Demanda
dados_tensao_rede = dados.iloc[:,13:14] # Pega a Tensao da Rede
dados_categoria_tensao = dados.iloc[:,14:15] # Pega a Tensao por Categoria (Coluna Criada Anteriormente)


dados_radiacao_normalizados, scaler_radiacao = normalizar (dados_radiacao) # Pega a Radiacao normalizada e o scaler dela
dados_temp_celula_normalizados, scaler_temp_celula = normalizar (dados_temp_celula) # Pega a Temperatura da Celula normalizada e o scaler dela
dados_demanda_normalizados, scaler_demanda = normalizar (dados_demanda) # Pega a Demanda normalizada e o scaler dela
dados_tensao_rede_normalizados, scaler_tensao_rede = normalizar (dados_tensao_rede) # Pega a Tensao da Rede normalizada e o scaler dela


# ## COMEcA O TREINO E CHAMDA AS FUNCOES

# In[ ]:


# ESSE EXEMPLO USA APENAS 1 VAEL DE PREDM A LSTM

# Carrega os dados de treino / teste para cada vael
# Radiacao / Temperatura Celula / Demanda / Tensao da Rede / Categoria_Tensao


                            # ESSE EXEMPLO USA 4 VARS DE PREDIcaO
    
# LSTM COM 4 ENTRADAS POR VEZ (AS 4 WAVELETS DA RADIACAO, COMO SAiDA O VALOR DA RADIACAO SEM NORMALIZACAO)

#data_wavelet = pd.read_csv('dados_wavelet.csv')
data_wavelet = pd.read_csv('dataset_multvariavel_alta_variabilidade_pandas.csv')
data_wavelet = data_wavelet.iloc[:,1:]
data_wavelet = np.array (data_wavelet)

# Separa os 75% para treino e 25% para teste
porcentagem_test = 0.25 # Escolhe quantos % dos dados ados ficam para treino

dados_lidos_lista = [20, 60,100]
dados_predito_lista = [1,2,5]
porcentagem_test = 0.25
#tamanho_kernel_lista = [2,4]
lista_dados_entrada = []
lista_resultados_erros = []
for predicao_dados in dados_predito_lista:
    for dados_lidos in dados_lidos_lista:
        forecast = predicao_dados
        dados_predito = predicao_dados
        # Junta os dados
        #dataset_multvariavel = np.hstack((cA1, cA2, cA3, cA4, data))
        dataset_multvariavel = data_wavelet

        # Normalizacao dos dados multvariaveis
        from sklearn.preprocessing import MinMaxScaler
        dataset_multvariavel = dataset_multvariavel.astype ('float32')
        scaler_dataset_multvariavel = MinMaxScaler (feature_range=(0,1))
        
        # NORMALIZA TODOS OS DADOS
        dataset_multvariavel_normalizados = scaler_dataset_multvariavel.fit_transform (dataset_multvariavel)

        # Separar Treino e Teste
        test_size = int (len (dataset_multvariavel_normalizados)*porcentagem_test)
        train_size = int (len(dataset_multvariavel_normalizados) - test_size)
        train, test = dataset_multvariavel_normalizados[0:train_size], dataset_multvariavel_normalizados[train_size:len(dataset_multvariavel_normalizados)]



        # Separa a quantidade de dados lidos para predicao e os dados de treino/teste de X e Y

        X_train, y_train = separar_dados_multvariavel(train, dados_lidos, forecast)
        X_test, y_test = separar_dados_multvariavel(test, dados_lidos, forecast)
        #y_train = y_train.reshape (-1,1)
        #y_test = y_test.reshape (-1,1)
        print ("\n\nFormato dos dados:")
        print ("X Treino: ", X_train.shape)
        print ("X Teste: ", X_test.shape)
        print ("Y Treino: ", y_train.shape)
        print ("Y Teste: ", y_test.shape)

    
        modelo_treinado = treinar_rede_bidirecional (X_train, y_train, dados_lidos,dados_predito)

        # Salva os dados com o nome abaixo
        #nome_modelo = 'Modelos_Tensao/modelo_bidirecional_radiacao_wavelet_multvariado_camada_3_lendo_'+str (dados_lidos)+'_predicao_'+str (dados_predito)                                  
        nome_modelo = 'Modelos_Tensao/modelo_bidirecional_tensao_wavelet_multvariado_alta_variabilidade_camada_3_lendo_'+str (dados_lidos)+'_predicao_'+str (dados_predito)                                  
        salvar_modelo (nome_modelo, modelo_treinado)


    # Transforma o teste para comprar (VALOR REAL)
    #test = scaler_radiacao.inverse_transform(y_test)

    # Aplica o modelo para depois desnormalizar (VALOR PREDITO)
    #test_predict = modelo_treinado.predict(X_test, verbose=0)
    #predictions = scaler_radiacao.inverse_transform(test_predict)
	

    # Calcula os erros de Valor Real x Valor Predito
    #erros = calcular_erros (test,predictions)

    # Salva os erros numa lista dados_lidos, dados_predito e erros
    #lista_dados_entrada.append (dados_lidos)
    #lista_dados_entrada.append (dados_predito)
    #lista_resultados_erros.append (erros)
    
 
